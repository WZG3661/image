# Image

#### 介绍
-   typora图床及图片上传脚本

#### 使用说明

1.  修改upload.sh中的本地仓库地址、远程Git仓库链接

2.  Typora偏好设置——图像——上传服务设定：选择Custom Command，命令为

    ```shell
    sh C:\\Users\\10147\\Pictures\\image\\upload.sh
    ```

