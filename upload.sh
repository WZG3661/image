if [[ $# -eq 0 ]];then
        echo "No Image Input"
        exit 2
fi

# 本地仓库地址
local_repo_path="C:\\Users\\10147\\Pictures\\image"
# 远程Git仓库链接
origin_repo_path="https://gitee.com/WZG3661/image/raw/master/img/"

cd $local_repo_path

# 拷贝图片到本地git仓库并打印链接
img_path=""
while [[ $# -gt 0 ]]
do
        path_raw=$1
        img_path=$path_raw
        shift
        
        # 判断是否是完整路径（可能含有空格）
        img_type=${path_raw##*.}
        while [[ $# -gt 0 && "${img_type}" != "jpg" && "${img_type}" != "jpeg" && "${img_type}" != "png" && "${img_type}" != "bmp" && "${img_type}" != "gif" && "${img_type}" != "tiff" && "${img_type}" != "svg" ]]
        do
                img_path="$img_path $1"
                img_type=${img_path##*.}
                shift
        done
        
        # 拷贝
        cp "$img_path" ./img/

        # 打印图片链接
        echo ${origin_repo_path}${img_path##*[\\\/]}
        img_path=""
done

# Git上传到远程仓库
git add img/
git commit -m "upload image"
git push